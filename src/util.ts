import { fuzzyFilter } from 'fuzzbunny';

export function filter<T>(s: string, v: T, strGetter: (t: T) => string): boolean {
    s = s?.trim();
    if (!s) {
        return true;
    }

    const removeSpaceFilter = s.replaceAll(' ', '');
    const splitSpaceFilter = s.split(' ');

    const name = strGetter(v);
    if (!name) {
        // ?
        return true;
    }

    const _splitSpaceFilter = splitSpaceFilter.slice();
    const removeSpaceName = name.replaceAll(' ', '');
    const splitSpaceName = name.split(' ');

    const cond1 = removeSpaceName?.includes(removeSpaceFilter);
    const cond2 = splitSpaceName.reduce((p, x) => {
        for (let i = 0; i < _splitSpaceFilter.length; i++) {
            if (x.includes(_splitSpaceFilter[i])) {
                _splitSpaceFilter.splice(i, 1);
                return p + 1;
            }
        }

        return p;
    }, 0) == splitSpaceFilter.length;

    if (cond1 || cond2) {
        return true;
    }

    return false;
}

export function filterList<T>(s: string, list: T[], strGetter: (t: T) => string): T[] {
    s = s?.trim().toLowerCase();
    if (!s) {
        return list;
    }

    const filtered: T[] = [];

    const removeSpaceFilter = s.replaceAll(' ', '');
    const splitSpaceFilter = s.split(' ');
    for (const v of list) {
        const name = strGetter(v).toLowerCase();
        if (!name) {
            // ?
            continue;
        }

        const _splitSpaceFilter = splitSpaceFilter.slice();
        const removeSpaceName = name.replaceAll(' ', '');
        const splitSpaceName = name.split(' ');

        const cond1 = removeSpaceName?.includes(removeSpaceFilter);
        const cond2 = splitSpaceName.reduce((p, x) => {
            for (let i = 0; i < _splitSpaceFilter.length; i++) {
                if (x.includes(_splitSpaceFilter[i])) {
                    _splitSpaceFilter.splice(i, 1);
                    return p + 1;
                }
            }

            return p;
        }, 0) == splitSpaceFilter.length;

        if (cond1 || cond2) {
            filtered.push(v);
        }
    }

    return filtered;
}

export function filterList2<T>(s: string, list: T[], strGetter: (t: T) => string): T[] {
    s = s?.trim();
    if (!s) {
        return list;
    }

    for (const v of list) {
        (v as any).__str = strGetter(v);
    }

    return fuzzyFilter(list, s, { fields: ['__str'] as any }).map(v => v.item);
}