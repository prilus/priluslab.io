import { GrpcWebFetchTransport } from "@protobuf-ts/grpcweb-transport";
import { ref, Ref } from 'vue';
import brotliPromise from 'brotli-dec-wasm';

import { mabiapiClient } from '@/protos/main.client';
import { region } from '@/store';
import { ResourceData, ResourceVersion } from '@/protos/resourcedata';

export const apiUrl = ref(import.meta.env.VITE_APP_APISERVER as string || "https://mabiapi.pril.cc/");
export const resUrl = ref(import.meta.env.VITE_APP_RESSERVER as string || "https://mabires.pril.cc/");
export const texUrl = ref(import.meta.env.VITE_APP_TEXSERVER as string || "https://mabitex.pril.cc/");

let loadingCount: Ref<number>;

export function resVerCall(path: string, opt?: HttpCallOpt): Promise<ResourceVersion> {
    return httpCall<ResourceVersion>(`${resUrl.value}${path}`, opt);
}

export async function resDataCall(path: string, opt?: HttpCallOpt): Promise<ResourceData> {
    const buf = await httpCallRaw(`${resUrl.value}${path}`, opt);
    
    try {
        loadingCount.value++;
        const brotli = await brotliPromise;
        const u8 = new Uint8Array(buf);
        
        const dec = brotli.decompress(u8);
        return ResourceData.fromBinary(dec);
    }
    finally {
        loadingCount.value--;
    }
}

export type HttpCallOpt = {
    disableLoading?: boolean;
    reload?: boolean;
};

async function httpCall<T>(url: string, opt?: HttpCallOpt): Promise<T> {
    await setLoadingCount();

    const buf = await httpCallRaw(url, opt);
    const text = new TextDecoder('utf-8').decode(buf);
    return JSON.parse(text);
}

async function httpCallRaw(url: string, opt?: HttpCallOpt): Promise<ArrayBuffer> {
    await setLoadingCount();

    try {
        if (!opt?.disableLoading) {
            loadingCount.value++;
        }

        const r = await fetch(url, {
            cache: opt?.reload ? 'reload' : undefined,
        });
        const buf = await r.arrayBuffer();
        if (r.status != 200) {
            throw new Error(`${r.status} ${new TextDecoder('utf-8').decode(buf)}`);
        }

        return buf;
    }
    finally {
        if (!opt?.disableLoading) {
            loadingCount.value--;
        }
    }
}

export * from '@/protos/main';

let transport = new GrpcWebFetchTransport({
    baseUrl: apiUrl.value,
});

export const apiClientRaw = new mabiapiClient(transport);

export const apiClient = new Proxy(apiClientRaw, {
    get(target, prop) {
        const origMethod = (target as any)[prop];
        if (typeof origMethod == 'function') {
            return async function (...args: any) {
                await setLoadingCount();

                try {
                    loadingCount.value++;

                    const result = await origMethod.apply(target, args);
                    return result;
                }
                finally {
                    loadingCount.value--;
                }
            }
        }
    }
});

// 순환참조 제거용
async function setLoadingCount() {
    if (loadingCount) {
        return;
    }

    const { loadingCount: _loadingCount } = await import('@/store');
    loadingCount = _loadingCount;
}

export async function init(): Promise<void> {
    if (import.meta.env.VITE_APP_APISERVER) {
        return;
    }

    const urlSets: [string, string, string][] = [
        ['https://mabiapi.pril.cc/', 'https://mabires.pril.cc/', 'https://mabitex.pril.cc/'],
        ['https://mabiapi2.pril.cc/', 'https://mabires2.pril.cc/', 'https://mabitex2.pril.cc/'],
        ['https://mabiapi3.pril.cc/', 'https://mabires3.pril.cc/', 'https://mabitex3.pril.cc/'],
        ['https://mabiapi4.pril.cc/', 'https://mabires4.pril.cc/', 'https://mabitex4.pril.cc/'],
    ];

    const startAt = Date.now();
    let isInitDone = false;
    let latestUpdateAt = 0;

    const promises = urlSets.map(urlSet => httpCall<ResourceVersion>(`${urlSet[1]}resourceversion/${region.value}/${region.value}_resourceversion.json`, {
        disableLoading: true,
        reload: true,
    }).catch(() => undefined).then(v => {
        if (!v || isInitDone) {
            return;
        }

        if (v.CreatedAt <= latestUpdateAt) {
            return;
        }

        [apiUrl.value, resUrl.value, texUrl.value] = urlSet;
        latestUpdateAt = v.CreatedAt;

        transport = new GrpcWebFetchTransport({
            baseUrl: apiUrl.value,
        });

        (apiClientRaw as any)._transport = transport;

        console.log(`set api server: ${apiUrl.value} (${Date.now() - startAt}ms, ${v.CreatedAt})`);
    }));

    // wait for request start
    await new Promise<void>(resolve => setTimeout(resolve, 100));

    promises.push(new Promise<void>(resolve => setTimeout(() => {
        isInitDone = true;
        if (!latestUpdateAt) {
            console.log('default api server');
        }

        resolve();
    }, 400)));

    return Promise.any(promises);
}