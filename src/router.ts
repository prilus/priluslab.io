import { createWebHistory, createRouter } from "vue-router";
import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        name: "home",
        component: () => import("./components/home.vue"),
    },
    {
        path: "/userinfo/:name?",
        name: "userinfoByName",
        component: () => import("./components/userInfo.vue"),
    },
    {
        path: "/charsimul",
        name: "characterSimulator",
        component: () => import("./components/characterSimulator.vue"),
    },
    {
        path: "/production",
        name: "productionList",
        component: () => import("./components/productionList.vue"),
    },
    {
        path: "/mileageshop",
        name: "mileageShop",
        component: () => import("./components/mileageShop.vue"),
    },
    {
        path: "/race",
        name: "races",
        component: () => import("./components/raceList.vue"),
    },
    {
        path: "/miniature",
        name: "miniatures",
        component: () => import("./components/miniatureList.vue"),
    },
    {
        path: "/barter",
        name: "barters",
        component: () => import("./components/barterList.vue"),
    },
    {
        path: "/title",
        name: "titles",
        component: () => import("./components/titleList.vue"),
    },
    {
        path: "/food",
        name: "foods",
        component: () => import("./components/foodList.vue"),
    },
    {
        path: '/item/:id?',
        name: 'itemById',
        component: () => import("./components/itemList.vue"),
    },
    {
        path: '/skill/:id?',
        name: 'skillsByCategory',
        component: () => import("./components/skillList.vue"),
    },
    {
        path: '/optionset',
        name: 'optionSets',
        component: () => import("./components/optionSetList.vue"),
    },
    {
        path: '/metalware/:id?',
        name: 'metalwareById',
        component: () => import("./components/metalwareList.vue"),
    },
    {
        path: '/itemupgrade/:id?',
        name: 'itemUpgradeById',
        component: () => import("./components/itemUpgradeList.vue"),
    },
    {
        path: '/randomtable/:id?',
        name: 'randomTableById',
        component: () => import("./components/randomTableList.vue"),
    },
    {
        path: '/achievement/:id?',
        name: 'achievementsByCategoryId',
        component: () => import("./components/achievementList.vue"),
    },
    {
        path: '/charcond',
        name: 'charconds',
        component: () => import("./components/characterConditionList.vue"),
    },
    {
        path: '/talent',
        name: 'talents',
        component: () => import("./components/talentList.vue"),
    },
    {
        path: '/pet',
        name: 'pets',
        component: () => import("./components/petList.vue"),
    },
    {
        path: '/totem',
        name: 'totems',
        component: () => import("./components/totemList.vue"),
    },
    {
        path: '/echostone',
        name: 'echostones',
        component: () => import("./components/echoStoneList.vue"),
    },
    {
        path: '/weaponskin',
        name: 'weaponskins',
        component: () => import("./components/weaponSkinList.vue"),
    },
    {
        path: '/auction/:query?',
        name: 'auctionHouseByQuery',
        component: () => import("./components/auctionHouse.vue"),
    },
    {
        path: '/multiclass',
        name: 'multiClass',
        component: () => import("./components/multiClassList.vue"),
    },
    {
        path: '/setitem',
        name: 'setItems',
        component: () => import("./components/setItemList.vue"),
    },
    {
        path: '/colortable/:id?',
        name: 'colorTableById',
        component: () => import("./components/colorTableList.vue"),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
