import { createApp } from "vue";
import App from "@/App.vue";
import router from "@/router";
import * as store from '@/store';

// vuetify
import 'vuetify/styles';
import { createVuetify } from 'vuetify';
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg';

const vuetify = createVuetify({
    theme: {
        defaultTheme: 'dark',
        themes: {
            light: {
                dark: false,
                colors: {
                    "background": "#FFFFFF",
                    "surface": "#FFFFFF",
                    "surface-bright": "#FFFFFF",
                    "surface-variant": "#424242",
                    "on-surface-variant": "#EEEEEE",
                    "primary": "#6200EE",
                    "primary-darken-1": "#3700B3",
                    "secondary": "#03DAC6",
                    "secondary-darken-1": "#018786",
                    "error": "#B00020",
                    "info": "#2196F3",
                    "success": "#4CAF50",
                    "warning": "#FB8C00",
                    "on-background": "#000",
                    "on-surface": "#000",
                    "on-surface-bright": "#000",
                    "on-primary": "#fff",
                    "on-primary-darken-1": "#fff",
                    "on-secondary": "#000",
                    "on-secondary-darken-1": "#fff",
                    "on-error": "#fff",
                    "on-info": "#fff",
                    "on-success": "#fff",
                    "on-warning": "#fff"
                },
            },
            dark: {
                dark: true,
                colors: {
                    "background": "#121212",
                    "surface": "#212121",
                    "surface-bright": "#ccbfd6",
                    "surface-variant": "#a3a3a3",
                    "on-surface-variant": "#424242",
                    "primary": "#BB86FC",
                    "primary-darken-1": "#3700B3",
                    "secondary": "#03DAC5",
                    "secondary-darken-1": "#03DAC5",
                    "error": "#CF6679",
                    "info": "#2196F3",
                    "success": "#4CAF50",
                    "warning": "#FB8C00",
                    "on-background": "#fff",
                    "on-surface": "#fff",
                    "on-surface-bright": "#000",
                    "on-primary": "#fff",
                    "on-primary-darken-1": "#fff",
                    "on-secondary": "#000",
                    "on-secondary-darken-1": "#000",
                    "on-error": "#fff",
                    "on-info": "#fff",
                    "on-success": "#fff",
                    "on-warning": "#fff"
                },
            }
        },
    },
    icons: {
        defaultSet: 'mdi',
        aliases,
        sets: {
            mdi,
        },
    },
});

// setting router
router.beforeEach((_1, _2, next) => {
    store.loadingCount.value++;
    next();

    return true;
});

router.afterEach(() => {
    store.loadingCount.value--;
});

router.onError((err) => {
    console.error(err);
    if (err instanceof Error && err.message.includes('Failed to fetch')) {
        location.reload();
    }
});

const app = createApp(App);

// register global variables
for (const _key in store) {
    const key = _key as keyof typeof store;
    app.provide(key, store[key]);
}

app.config.errorHandler = (err) => {
    alert(err);
    console.error(err);
}

app
    .use(router)
    .use(vuetify)
    .mount("#app");
