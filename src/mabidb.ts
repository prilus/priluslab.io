import { resVerCall, resDataCall, init as initApi, apiClientRaw } from '@/apicall';
import { ResourceData as ResourceDataTier1, ResourceVersion } from '@/protos/resourcedata';

type OnlyArray<T> = T extends Array<infer V> ? V[] : never;

type ListTypeKeyof<T> = {
    [P in keyof T]-?: T[P] extends OnlyArray<T[P]> ? P : never;
}[keyof T];

// 나중에 옴기기
type ResourceDataTier2 = {
    AuctionItemLowestPriceList: {
        Id: number;
        Price: number;
    }[];
    AuctionEnchantScrollLowestPriceList: {
        Id: number;
        Price: number;
    }[];
    AuctionPetSealLowestPriceList: {
        Id: number;
        Price: number;
    }[];
}

type ResourceData = ResourceDataTier1 & ResourceDataTier2;

/*
tier1: res에서 가져오는 데이터
* 필수
* resourceVersion 확인 후 갱신

tier2: api에서 가져오는 데이터
* optional
* resourceVersion 확인 할 때 같이 갱신
* background에서 갱신
*/
export class MabiDB {
    private static dbName = "prilus_mabi_db";
    private static dataTableName = "data";
    private static versionTableName = "version";
    private static updateCheckInterval = 300;

    private dbHandle?: IDBDatabase;

    private cachedRegionString: Record<string, string> = {};
    private cachedLangString: Record<string, string> = {};
    private cachedAuctionItemLowestPrice: Record<number, number> = {};

    public constructor(private region: string, private lang: string) {
    }

    async open(): Promise<void> {
        this.dbHandle?.close();
        this.dbHandle = undefined;

        // this.isLoading.value = true;

        await new Promise<void>((resolve, reject) => {
            const dbReq = indexedDB.open(MabiDB.dbName, 2);

            dbReq.onupgradeneeded = () => {
                console.log("MabiDB.open: onupgradeneeded");

                const dbHandle = dbReq.result;
                dbHandle.createObjectStore(MabiDB.dataTableName);
                dbHandle.createObjectStore(MabiDB.versionTableName);
            };

            dbReq.onsuccess = () => {
                console.log("MabiDB.open: onsuccess");

                this.dbHandle = dbReq.result;
                resolve();
            }

            dbReq.onerror = () => {
                console.log("MabiDB.open: onerror");

                indexedDB.deleteDatabase(MabiDB.dbName);
                reject(dbReq.error);
            }
        });

        await this.checkUpdate(this.region);
        if (this.region !== this.lang) {
            await this.checkUpdate(this.lang);
        }

        await this.reloadStringTable();
        await this.reloadAuctionItemLowestPrice();
    }

    public async tryOpen(): Promise<void> {
        if (this.dbHandle) {
            return;
        }

        await initApi();
        await this.open();
    }

    public getData<T extends keyof ResourceData>(key: T, region = this.region): Promise<ResourceData[T]> {
        const rotx = this.dbHandle?.transaction(MabiDB.dataTableName, "readonly");
        if (!rotx) {
            throw new Error("MabiDB.getData: dbHandle is not ready");
        }

        const store = rotx.objectStore(MabiDB.dataTableName);
        const getReq = store.get(`${key}_${region}`);
        return new Promise((resolve, reject) => {
            getReq.onsuccess = () => {
                resolve(getReq.result as ResourceData[T]);
            }

            getReq.onerror = () => {
                reject(getReq.error);
            }
        });
    }

    public getSortedListData<T extends ListTypeKeyof<ResourceData>>(key: T, region = this.region): Promise<ResourceData[T]> {
        const rotx = this.dbHandle?.transaction(MabiDB.dataTableName, "readonly");
        if (!rotx) {
            throw new Error("MabiDB.getData: dbHandle is not ready");
        }

        const store = rotx.objectStore(MabiDB.dataTableName);
        const getReq = store.get(`${key}_${region}`);
        return new Promise((resolve, reject) => {
            getReq.onsuccess = () => {
                const l = getReq.result as ResourceData[T] || [];

                l.sort((a, b) => {
                    const aKey = keyGetter(key, a);
                    const bKey = keyGetter(key, b);

                    return aKey - bKey;
                });

                resolve(l);
            }

            getReq.onerror = () => {
                reject(getReq.error);
            }
        });
    }

    private getVersion(type: string, region: string): Promise<number> {
        const rotx = this.dbHandle?.transaction(MabiDB.versionTableName, "readonly");
        if (!rotx) {
            throw new Error("MabiDB.getVersion: dbHandle is not ready");
        }

        const store = rotx.objectStore(MabiDB.versionTableName);
        const getReq = store.get(`${type}_${region}`);
        return new Promise((resolve, reject) => {
            getReq.onsuccess = () => {
                resolve(+(getReq.result || '0'));
            }

            getReq.onerror = () => {
                reject(getReq.error);
            }
        });
    }

    public async checkUpdate(region: string): Promise<void> {
        const latestVersionCheckAt = await this.getVersion("LatestVersionCheckAt", region) || 0;
        const isTooOldVersionCheck = (Date.now() / 1000) - MabiDB.updateCheckInterval > latestVersionCheckAt;

        console.log("MabiDB.checkUpdate: isTooOldVersionCheck", latestVersionCheckAt, region)

        if (!isTooOldVersionCheck) {
            console.log("MabiDB.checkUpdate: not need update", region);
            return;
        }

        // ignore promise
        this.updateTier2(region);

        const latestVersion = (await loadServerResourceVersion(region))?.CreatedAt ?? 0;
        const currentVersion = await this.getVersion("CreatedAt", region) ?? 0;

        console.log("MabiDB.checkUpdate: latestVersion", latestVersion, "currentVersion", currentVersion, region)

        if (latestVersion <= currentVersion) {
            this.saveVersion("LatestVersionCheckAt", region, Date.now() / 1000);

            console.log("MabiDB.checkUpdate: not need update", region);
            return;
        }

        await this.updateTier1(region);
    }

    public async forceUpdate(): Promise<void> {
        await this.updateTier1(this.region);
        this.updateTier2(this.region);
        if (this.region !== this.lang) {
            await this.updateTier1(this.lang);
            this.updateTier2(this.lang);
        }
    }

    private async updateTier1(region: string): Promise<void> {
        const { loadingCount } = await import('@/store');
        try {
            loadingCount.value++;

            const data = await loadServerResourceDataTier1(region);
            await this.saveDataAllTier1(data, region);

            const version = await loadServerResourceVersion(region);
            await this.saveVersion("CreatedAt", region, data.Version?.CreatedAt || 0);
            await this.saveVersion("LatestVersionCheckAt", region, Date.now() / 1000);

            await this.reloadStringTable();
            console.log("MabiDB.update: updated", version.CreatedAt, region);
        }
        finally {
            loadingCount.value--;
        }
    }

    private async updateTier2(region: string): Promise<void> {
        // kr or krt only?        
        region;

        try {
            {
                const { response: r } = await apiClientRaw.tier2Data({});
                const item = Object.entries(r.AuctionItemLowestPriceMap).map(([k, v]) => ({ Id: +k, Price: v }));
                const enchantScroll = Object.entries(r.AuctionEnchantScrollLowestPriceMap).map(([k, v]) => ({ Id: +k, Price: v }));
                const petSeal = Object.entries(r.AuctionPetSealLowestPriceMap).map(([k, v]) => ({ Id: +k, Price: v }));
    
                await this.saveDataTier2("AuctionItemLowestPriceList", item, region);
                await this.saveDataTier2("AuctionEnchantScrollLowestPriceList", enchantScroll, region);
                await this.saveDataTier2("AuctionPetSealLowestPriceList", petSeal, region);
            }
        }
        catch (e) {
            // log only
            console.warn("MabiDB.updateTier2: auctionItemLowestPriceList error", e);
        }
    }

    private clearData(): Promise<void> {
        const dbHandle = this.dbHandle;
        if (!dbHandle) {
            throw new Error("MabiDB.saveDataAll: dbHandle is not ready");
        }

        const tx = dbHandle.transaction(MabiDB.dataTableName, "readwrite");
        const store = tx.objectStore(MabiDB.dataTableName);

        return new Promise((resolve, reject) => {
            const clearReq = store.clear();
            clearReq.onerror = () => {
                console.error("MabiDB.clearData: clearReq.onerror", clearReq.error);
                reject(clearReq.error);
            }

            clearReq.onsuccess = () => {
                tx.commit();
                resolve();
            }
        });
    }

    private async saveDataTier2<T extends keyof ResourceDataTier2>(key: T, data: ResourceDataTier2[T], region: string): Promise<void> {
        const dbHandle = this.dbHandle;
        if (!dbHandle) {
            throw new Error("MabiDB.saveDataAll: dbHandle is not ready");
        }
        
        const tx = dbHandle.transaction(MabiDB.dataTableName, "readwrite");
        const store = tx.objectStore(MabiDB.dataTableName);
        
        const putReq = store.put(data, `${key}_${region}`);
        await new Promise<void>((resolve, reject) => {
            putReq.onsuccess = () => {
                resolve();
            }

            putReq.onerror = () => {
                console.error("MabiDB.saveData: putReq.onerror", putReq.error);
                reject(putReq.error);
            }
        });
     
        tx.commit();
    }

    private async saveDataAllTier1(data: ResourceDataTier1, region: string): Promise<void> {
        const dbHandle = this.dbHandle;
        if (!dbHandle) {
            throw new Error("MabiDB.saveDataAll: dbHandle is not ready");
        }

        const tx = dbHandle.transaction(MabiDB.dataTableName, "readwrite");
        const store = tx.objectStore(MabiDB.dataTableName);

        const promises: Promise<void>[] = [];
        const insertPromise = async (req: IDBRequest<IDBValidKey>) => {
            promises.push(new Promise<void>((resolve, reject) => {
                req.onsuccess = () => {
                    resolve();
                }

                req.onerror = () => {
                    console.error("MabiDB.saveDataAll: putReq.onerror", req.error);
                    reject(req.error);
                }
            }));
        }

        for (const _key in data) {
            const key = _key as keyof ResourceDataTier1;
            const putReq = store.put(data[key], `${key}_${region}`);
            await insertPromise(putReq);

            /*
            const list = data[key];
            if (key != 'StringTable' && list instanceof Array) {
                for (const v of list) {
                    const elemKey = `${key}_${region}.${keyGetter(key as any, v)}`;
                    const elemPutReq = store.put(v, elemKey);
                    await insertPromise(elemPutReq);
                }
            }
            */
        }

        await Promise.all(promises);

        tx.commit();
    }

    private async saveVersion(type: string, region: string, version: number): Promise<void> {
        const dbHandle = this.dbHandle;
        if (!dbHandle) {
            throw new Error("MabiDB.saveVersion: dbHandle is not ready");
        }

        const tx = dbHandle.transaction(MabiDB.versionTableName, "readwrite");
        const store = tx.objectStore(MabiDB.versionTableName);
        const putReq = store.put(version, `${type}_${region}`);
        await new Promise<void>((resolve, reject) => {
            putReq.onsuccess = () => {
                resolve();
            }

            putReq.onerror = () => {
                console.error("MabiDB.saveVersion: putReq.onerror", putReq.error);
                reject(putReq.error);
            }
        });

        tx.commit();
    }

    public getDataVersion(): Promise<number> {
        return this.getVersion("CreatedAt", this.region);
    }

    private async reloadStringTable(): Promise<void> {
        const region = await this.getData("StringTable") || [];
        const lang = await this.getData("StringTable", this.lang) || [];

        for (const { Id, Str } of region) {
            this.cachedRegionString[Id] = Str;
        }

        for (const { Id, Str } of lang) {
            this.cachedLangString[Id] = Str;
        }
    }

    private async reloadAuctionItemLowestPrice(): Promise<void> {
        const auctionItemLowestPrice = await this.getData("AuctionItemLowestPriceList") || [];
        this.cachedAuctionItemLowestPrice = auctionItemLowestPrice.reduce((acc, { Id, Price }) => {
            acc[Id] = Price;
            return acc;
        }, {} as Record<number, number>);
    }

    public getCurLangString(key: string): string {
        if (key == "None") {
            return key;
        }

        const langStr = this.cachedLangString[key];
        if (langStr) {
            return langStr;
        }

        const regionStr = this.cachedRegionString[key];
        if (regionStr) {
            return regionStr;
        }

        return key;
    }

    public getCurLangStrings(keys: string[]): string[] {
        return keys.map(key => this.getCurLangString(key));
    }

    // 0 -> 경매장에 매물이 없음, undefined -> 경매장 불가능 아이템
    public getAuctionItemLowestPrice(id: number): number | undefined {
        return this.cachedAuctionItemLowestPrice[id];
    }
}

async function loadServerResourceDataTier1(region: string): Promise<ResourceDataTier1> {
    const d = await resDataCall(`resourcedata/${region}/${region}_resourcedata.bin.br`);

    return d;
}

async function loadServerResourceVersion(region: string): Promise<ResourceVersion> {
    const d = await resVerCall(`resourceversion/${region}/${region}_resourceversion.json`);

    return d;
}

function keyGetter<T extends ListTypeKeyof<ResourceData>>(typeName: T, d: ResourceData[T][0]): number {
    // tier1
    if (typeName == 'AchievementList') {
        const achievement = d as ResourceData['AchievementList'][0];
        return achievement.Id;
    }
    else if (typeName == 'BarterList') {
        const barter = d as ResourceData['BarterList'][0];
        return barter.Id;
    }
    else if (typeName == 'CharCondList') {
        const charCond = d as ResourceData['CharCondList'][0];
        return charCond.Id;
    }
    else if (typeName == 'ColorTableList') {
        const color = d as ResourceData['ColorTableList'][0];
        return color.Id;
    }
    else if (typeName == 'EchoStoneAwakenAdjustByGradeList') {
        const adjust = d as ResourceData['EchoStoneAwakenAdjustByGradeList'][0];
        return adjust.Grade;
    }
    else if (typeName == 'EchoStoneAwakenAdjustByItemList') {
        const adjust = d as ResourceData['EchoStoneAwakenAdjustByItemList'][0];
        return adjust.ItemId;
    }
    else if (typeName == 'EchoStoneConvertAdditionalRewardList') {
        const reward = d as ResourceData['EchoStoneConvertAdditionalRewardList'][0];
        return reward.Id;
    }
    else if (typeName == 'EchoStoneList') {
        const echoStone = d as ResourceData['EchoStoneList'][0];
        return echoStone.Id;
    }
    else if (typeName == 'FoodList') {
        const food = d as ResourceData['FoodList'][0];
        return food.Id;
    }
    else if (typeName == 'ItemExtendCustomDataList') {
        const item = d as ResourceData['ItemExtendCustomDataList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemExtendMetalWareList') {
        const item = d as ResourceData['ItemExtendMetalWareList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemExtendSetItemDescList') {
        const item = d as ResourceData['ItemExtendSetItemDescList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemExtendTotemList') {
        const item = d as ResourceData['ItemExtendTotemList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemExtendUpgradeList') {
        const item = d as ResourceData['ItemExtendUpgradeList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemExtendWeaponSkinList') {
        const item = d as ResourceData['ItemExtendWeaponSkinList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemList') {
        const item = d as ResourceData['ItemList'][0];
        return item.Id;
    }
    else if (typeName == 'ItemUpgradeList') {
        const item = d as ResourceData['ItemUpgradeList'][0];
        return item.Id;
    }
    else if (typeName == 'MetalWareAbilityList') {
        const item = d as ResourceData['ItemExtendMetalWareList'][0];
        return item.Id;
    }
    else if (typeName == 'MetalWareItemList') {
        const item = d as ResourceData['MetalWareItemList'][0];
        return item.Id;
    }
    else if (typeName == 'MetalWareLevelList') {
        const item = d as ResourceData['MetalWareLevelList'][0];
        return item.Level;
    }
    else if (typeName == 'MiniatureList') {
        const item = d as ResourceData['MiniatureList'][0];
        return item.Id;
    }
    else if (typeName == 'MultiClassList') {
        const item = d as ResourceData['MultiClassList'][0];
        return item.Id;
    }
    else if (typeName == 'OptionSetList') {
        const item = d as ResourceData['OptionSetList'][0];
        return item.Id;
    }
    else if (typeName == 'PetList') {
        const item = d as ResourceData['PetList'][0];
        return item.Id;
    }
    else if (typeName == 'ProductionList') {
        const item = d as ResourceData['ProductionList'][0];
        return item.ItemId;
    }
    else if (typeName == 'RaceList') {
        const item = d as ResourceData['RaceList'][0];
        return item.Id;
    }
    else if (typeName == 'RandomTableList') {
        const item = d as ResourceData['RandomTableList'][0];
        return item.Id;
    }
    else if (typeName == 'SetItemDescElementList') {
        const item = d as ResourceData['SetItemDescElementList'][0];
        return item.Id;
    }
    else if (typeName == 'SkillList') {
        const item = d as ResourceData['SkillList'][0];
        return item.Id;
    }
    else if (typeName == 'SocialMotionList') {
        const item = d as ResourceData['SocialMotionList'][0];
        return item.Id;
    }
    else if (typeName == 'TalentList') {
        const item = d as ResourceData['TalentList'][0];
        return item.Id;
    }
    else if (typeName == 'TitleList') {
        const item = d as ResourceData['TitleList'][0];
        return item.Id;
    }
    // tier2
    else if (typeName == 'AuctionItemLowestPriceList') {
        const item = d as ResourceData['AuctionItemLowestPriceList'][0];
        return item.Id;
    }
    else if (typeName == 'AuctionEnchantScrollLowestPriceList') {
        const item = d as ResourceData['AuctionEnchantScrollLowestPriceList'][0];
        return item.Id;
    }
    else if (typeName == 'AuctionPetSealLowestPriceList') {
        const item = d as ResourceData['AuctionPetSealLowestPriceList'][0];
        return item.Id;
    }

    throw new Error(`keyGetter: unknown typeName ${typeName}`);
}