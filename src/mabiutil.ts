export function getHeightByAge(age: number): number {
    return ((age - 10) * (0.1434))
}

export function mabiRandom(min: number, max: number): number {
    if (min > max) {
        [min, max] = [max, min];
    }

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function mabiHumanReadablePrice(n: number): string {
    const kr = (): string => {
        const unitWords = ['', '만', '억', '조', '경'];
        const splitUnit = 10000;
        
        let r = "";
    
        for (let i = 0; n != 0; i++, n = Math.floor(n / splitUnit)) {
            if (i + 1 == unitWords.length) {
                r = `${n}${unitWords[i]} ${r}`;
                break;
            }
    
            const nn = n % splitUnit;
            if (nn != 0) {
                r = `${nn}${unitWords[i]} ${r}`;
            }
        }
    
        return r;
    }

    const en = (): string => {
        const unitWords = ['', 'K', 'M', 'B', 'T', 'Q'];
        const splitUnit = 1000;
        
        let r = "";
    
        for (let i = 0; n != 0; i++, n = Math.floor(n / splitUnit)) {
            if (i + 1 == unitWords.length) {
                r = `${n}${unitWords[i]} ${r}`;
                break;
            }
    
            const nn = n % splitUnit;
            if (nn != 0) {
                r = `${nn}${unitWords[i]} ${r}`;
            }
        }

        return r;
    }

    const lang = (navigator.language || '').toLowerCase();
    if (lang.includes('ko') || lang.includes('kr')) {
        return kr();
    }

    return en();
}