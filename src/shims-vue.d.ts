import { ComputedRef, Ref } from 'vue';

import { MabiDB } from '@/mabidb';
import type AppComponent from '@/App.vue';

declare module '@vue/runtime-core' {
    function inject(key: 'isLoading'): ComputedRef<boolean>;
    function inject(key: 'region'): Ref<string>;
    function inject(key: 'lang'): Ref<string>;
    function inject(key: 'regionList'): Ref<string[]>;
    function inject(key: 'db'): ComputedRef<MabiDB>;
    function inject(key: 'appComponent'): Ref<typeof AppComponent>;
    function inject(key: 'globalJsonDialog'): Ref<{
        show: boolean;
        json: string;
    }>;
    function inject(key: 'globalTooltip'): Ref<{
        show: boolean;
        parent: Element;
        text: string;
    }>;
}
