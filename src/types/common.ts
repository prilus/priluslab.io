export interface IdWithName {
    Id: number;
    Name: string;
}

export interface CharColor {
    Id: number;
    Name: string;
    Color: string;
}

export interface CharStyleInfo {
    Id: number;
    Name: string;
    Color?: string;
    AllowRaces?: number[];
    // SubId?: number;
	ColorCount?: number;
}

export interface UserEquip {
    Id: number;
    Name?: string;
    Colors?: string[];
}
