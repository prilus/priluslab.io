#/bin/bash

npx protoc --ts_out src/protos/ --ts_opt use_proto_field_name,ts_nocheck,eslint_disable,long_type_number \
    --proto_path src/protos/ src/protos/*.proto
